from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.
class Exam(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    archived = models.BooleanField(default=False)

    def absoluteURL(self):
        return reverse('exams:exam-detail', kwargs={'id': self.id})

    def assignURL(self):
        return reverse('exams:exam-assign', kwargs={'id': self.id})

    def archiveURL(self):
        return reverse('exams:exam-archive', kwargs={'id': self.id})

class Task(models.Model):
    task = models.TextField()
    score = models.IntegerField()
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)

class Assignment(models.Model):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE) 
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grade = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)

    def absoluteURL(self):
        return reverse('exams:assignment-details', kwargs={'id': self.id})

    def completeURL(self):
        return reverse('exams:assignment-complete', kwargs={'id': self.id})

class Solution(models.Model):
    finished = models.BooleanField(default=False)
    solution = models.TextField()
    score = models.DecimalField(decimal_places=2, max_digits=100, default=0)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)

    def editURL(self):
        return reverse('exams:solution-edit', kwargs={'id': self.id})
