from django.urls import path
from exams.views import (add_task_to_exam_form, assignment_complete, assignment_details, 
                         assign_exam_form, assignments_list, archive_exam, exams_list, solution_edit)

app_name = 'exams'
urlpatterns = [
    path('', exams_list, name='exams-list'),
    path('<int:id>/', add_task_to_exam_form, name='exam-detail'),
    path('<int:id>/assign', assign_exam_form, name='exam-assign'),
    path('<int:id>/archive', archive_exam, name='exam-archive'),

    path('assignments', assignments_list, name='assignments-list'),
    path('assignment_details/<int:id>', assignment_details, name='assignment-details'),
    path('assignment_complete/<int:id>', assignment_complete, name='assignment-complete'),

    path('solution/<int:id>', solution_edit, name='solution-edit')
]
