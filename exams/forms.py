from django import forms

from .models import Exam, Task, Assignment, Solution

class ExamForm(forms.ModelForm):
    class Meta:
        model = Exam
        fields = [
            'name',
            'description'
        ]

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            'task',
            'score'
        ]

class AssignmentForm(forms.ModelForm):
    class Meta:
        model = Assignment
        fields = [
            'user',
        ]

class SolutionForm(forms.ModelForm):
    class Meta:
        model = Solution
        fields = [
            'solution',
        ]
