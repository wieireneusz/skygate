from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.contrib.auth.decorators import login_required

from .models import Exam, Task, Assignment, Solution
from .forms import ExamForm, TaskForm, AssignmentForm, SolutionForm

# Create your views here.

@login_required
def exams_list(request):
    if not request.user.has_perm('exams.add_exam'):
        raise Http404
    exams = Exam.objects.filter(user=request.user, archived=False)
    form = ExamForm(request.POST or None)
    if form.is_valid():
        form = form.save(commit=False)
        form.user = request.user
        form.save()
        form = ExamForm()

    context = {
        'exams': exams,
        'form': form
    }

    return render(request, 'exam/list.html', context)

@login_required
def assignments_list(request):
    assignments = Assignment.objects.filter(user=request.user)

    context = {
        'assignments': assignments
    }

    return render(request, 'assignments/list.html', context)

@login_required
def add_task_to_exam_form(request, id):
    if not request.user.has_perm('exams.add_task'):
        raise Http404
    exam = get_object_or_404(Exam, id=id, archived=False)
    tasks = Task.objects.filter(exam=exam)
    form = TaskForm(request.POST or None)
    if form.is_valid():
        form = form.save(commit=False)
        form.exam = exam
        form.save()
        form = TaskForm()

    context = {
        'exam': exam,
        'tasks': tasks,
        'form': form
    }

    return render(request, 'exam/detail.html', context)  

@login_required
def assign_exam_form(request, id):
    if not request.user.has_perm('exam.add_assignment'):
        raise Http404
    exam = get_object_or_404(Exam, id=id, archived=False)
    tasks = Task.objects.filter(exam=id)
    assignments = Assignment.objects.filter(exam=id)
    form = AssignmentForm(request.POST or None)
    if form.is_valid():
        form = form.save(commit=False)
        form.exam = exam
        form.save()
        for task in tasks:
            solution = Solution.objects.create(solution='', task=task, score=0, assignment=form)
            solution.save()
        form = AssignmentForm()

    context = {
        'exam': exam,
        'assignments': assignments,
        'form': form
    }

    return render(request, 'exam/assign.html', context)

@login_required
def archive_exam(request, id):
    if not request.user.has_perm('exams.add_exam'):
        raise Http404
    exam = get_object_or_404(Exam, id=id, archived=False, user=request.user)
    if request.method == 'POST':
        exam.archived = True
        exam.save()
        return redirect('../')

    context = {
        'exam': exam,
    }

    return render(request, 'exam/archive.html', context)

@login_required
def assignment_details(request, id):
    assignment = get_object_or_404(Assignment, id=id)
    if not assignment.user == request.user:
        raise Http404
    solutions = Solution.objects.filter(assignment=assignment)

    context = {
        'assignment': assignment,
        'solutions': solutions
    }

    return render(request, 'assignments/details.html', context)

@login_required
def solution_edit(request, id):
    solution = get_object_or_404(Solution, id=id)
    if not solution.assignment.user == request.user:
        raise Http404
    if solution.task.exam.archived or solution.assignment.completed:
        raise Http404
    form = SolutionForm(request.POST or None, instance=solution)
    if form.is_valid():
        form.finished = True
        form.save()
        return redirect('../assignment_details/'+str(solution.assignment.id))

    context = {
        'solution': solution,
        'form': form,
    }

    return render(request, 'assignments/edit.html', context)

@login_required
def assignment_complete(request, id):
    assignment = get_object_or_404(Assignment, id=id)
    if not assignment.user == request.user:
        raise Http404
    if assignment.exam.archived or assignment.completed:
        raise Http404
    if request.method == 'POST':
        assignment.completed = True
        assignment.save()
        return redirect('../assignments')

    context = {
        'assignment': assignment,
    }

    return render(request, 'assignments/complete.html', context)
