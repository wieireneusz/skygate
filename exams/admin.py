from django.contrib import admin

from .models import Exam, Task, Assignment, Solution
# Register your models here.

admin.site.register(Exam)
admin.site.register(Task)
admin.site.register(Solution)
admin.site.register(Assignment)