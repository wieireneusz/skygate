from urllib.request import urlopen
import re

url= urlopen('https://pastebin.com/raw/3u4QViN8')

countValidSkyphrases= 0
for line in url:
    l= line.decode('utf-8').replace('\r\n', '')
    words= l.split(' ')
    unique= set(words)
    if len(words) == len(unique):
        countValidSkyphrases+= 1

print(countValidSkyphrases)


url2= urlopen('https://pastebin.com/raw/azc6e9fD')
url2File= url2.read().decode('utf-8')
match= re.findall(r'(-?\d+)', url2File)
digits= [int(d) for d in match]

print(sum(digits))