## SETUP 

First, let’s clone the repository with our code:
    git clone https://wieireneusz@bitbucket.org/wieireneusz/sg.git

Start a virtual environment:
    virtualenv venv -p python3

Initialize the virtualenv:
    source venv/bin/activate

Install the requirements:
    pip install Django==2.1.7

Now let’s migrate the database, collect the static files and create a super user:
    cd sg/ 
    python manage.py migrate
    python manage.py createsuperuser

Start project/server
    python manage.py runserver
